######################################################################
#                            Class ProteinDb                         #
# ______________________________________________________________ ____#
# Creates a protein database from a list of FastaEntry elements      #
# by which proteins are assigned to peptide sequences.               #
######################################################################

class ProteinDb:
    def __init__(self, FastaEntries=[]):  # Constructor must be called with a list of elements from type FastaProt.
        self.db = dict()
        self.includedProteins = FastaEntries  # Save provided proteins.
        self.put2DB()

    def __str__(self):
        return (self.db)

    def put2DB(self, offset=0):

        for i in range(offset, len(self.includedProteins)):
            peptides = self.includedProteins[i].getNormalPeptides()

            for peptide in peptides:
                if (peptide in self.db):
                    self.db[peptide].add(i)

                else:
                    self.db[peptide] = set()
                    self.db[peptide].add(i)

        return True

    def deflat(self, listoflists):
        r = list()
        for liste in listoflists:
            r += liste
        return r

    def getPeptides(self):  # Returns a list of all peptides within the database.
        return (self.db.keys())

    def getProteins(self):  # Returns a set of all protein within the database.
        referenceset = list(set().union(self.deflat(self.db.values())))
        return ([self.includedProteins[i] for i in referenceset])

    def getHeader(self):  # Returns a set of protein headers.
        referenceset = list(set().union(self.deflat(self.db.values())))
        return ([self.includedProteins[i].header for i in referenceset])

    def difference(self,
                   otherProtDB):  # Creates a subset database in which all unique peptides are returned that do not occur in the specified external database.
        keys = set(self.db.keys()).difference(otherProtDB.db.keys())
        return self.keys2subDB(keys)

    def intersection(self, otherProtDB):  # Creates a subset database with all peptides which occur in both databases.
        keys = set(self.db.keys()).intersection(set(otherProtDB.db.keys()))
        return self.keys2subDB(keys)

    def keys2subDB(self, keys):  # Creates a subset directory with all keys.
        ndb = ProteinDb()
        ndb.db = dict((key, value) for key, value in self.db.items() if key in keys)
        ndb.includedProteins = self.includedProteins
        return ndb

    def coverage(self):  # Calculates the protein sequence coverage based on the occurring peptides.
        cov = list()
        for elem in self.includedProteins:
            cov.append([False] * len(elem.sequence))

        for key in self.db.keys():  # Iterate each peptide within the database.
            prots = self.db[key]
            l = len(key)
            for ref in prots:  # Begin with each protein for which peptide X was assigned.
                start = 0  # Staring value for the search of the peptide in the protein sequence.
                while (True):
                    start = self.includedProteins[ref].sequence.replace('I', 'L').find(key, start)  # Find peptide in protein sequence.
                    if (start >= 0):
                        cov[ref][start:start + l] = [True] * l
                    if (start == -1):
                        break;
                    start += 1  # Search for further occurrence of peptide in the protein.
        return (
        [(self.includedProteins[i], (float(sum(cov[i])) / len(cov[i]) * 100)) for i in range(len(self.includedProteins))
         if (sum(cov[i]))])  # Calculate relative coverage

    def trim(self):  # Removes all unused proteins from the included proteins database.
        self.includedProteins = self.getProteins()
        self.db = dict()
        self.put2DB(0)
