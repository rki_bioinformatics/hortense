import csv

######################################################################
######################################################################
####                MSGF-Plus Preprocessing                       ####
######################################################################
######################################################################

## MSGF-Plus TSV file constants (corresponding to header in output file)
const = {
    '#SpecFile': 0,
    'SpecID': 1,
    'ScanNum': 2,
    'Title': 3,
    'FragMethod': 4,
    'Precursor': 5,
    'IsotopeError': 6,
    'PrecursorError(ppm)': 7,
    'Charge': 8,
    'Peptide': 9,
    'Protein': 10,
    'DeNovoScore': 11,
    'MSGFScore': 12,
    'SpecEValue': 13,
    'EValue': 14,
    'QValue': 15,
    'PepQValue': 16
}


#################################################################################
# remove_double_Spectra()                                                       #
# To reduce the amount of false-positives, MS-GF+ is executed twice on the same #
# spectra. This function receives to strings and returns the unique spectra for #
# the donor in a single list without header.                                    #
#################################################################################

def remove_double_Spectra(acceptorTsv, donorTsv):  # Remove duplicate spectra

    # Sanity checks
    if (not (type(acceptorTsv) is type(donorTsv) and (type(donorTsv) is list or type(
            donorTsv) is str))):
        raise ValueError("AcceptorTsv and DonorTsv don't have the same data type or aren't equal to list or str!")

    if (type(acceptorTsv) is str):
        acceptorTsv = [acceptorTsv]  # Put acceptor and donor to strings in list format
        donorTsv = [donorTsv]

    if (len(acceptorTsv) != len(donorTsv)):
        raise ValueError("AcceptorTsv and DonorTsv don't have the same length!")

    titleset = set()  # Saves all spectra occurring in acceptor
    for afile in acceptorTsv:  # For each file in acceptor list
        f = open(afile, 'r')
        tsv = csv.reader(f, delimiter="\t")  # Use CSV reader using tab-delimited format
        for row in tsv:
            titleset.add(row[const['Title']])
        f.close()

    ret = list()

    for afile in donorTsv:  # For each file in donor list
        f2 = open(afile, 'r')
        tsv = csv.reader(f2, delimiter="\t")  # Use CSV reader using tab-delimited format
        for row in tsv:
            if (not (row[const['Title']] in titleset)):
                ret.append(row)
        f2.close()

    return ret


###############################################################
# Tsv2List()                                                  #
# Reads in TSV file and transforms it to 2-dim list.           #
###############################################################


def Tsv2List(filepath):
    f = open(filepath, "r")
    tsvl = list()
    tsv = csv.reader(f, delimiter="\t")  # Use CSV reader using tab-delimited format
    for row in tsv:
        tsvl.append(row)
    f.close()
    return tsvl


#########################################################################
# TsvFilter2()                                                          #
# filtervalue   - By which variable name is filtered                    #
# filtercutoff  - Filtering threshold at which is filtered              #
# filterOff     - Disactivates the filtering function                   #
# header        - Is header available?                                  #
# Returns a dictionary: keys are peptides, values are the number of     #
# peptides above the filtering threshold.                               #
#########################################################################
def TsvFilter2(tsvList, filtervalue="QValue", filtercutoff=0.01, filterOff=False,
               header=True):  # Filter for a 'filtervalue' better than 'filtercutoff' returns a dict() with count of the Peptides
    peptidindex = const["Peptide"]  # Peptide field access
    filterindex = const[filtervalue]  # Filter value field access
    rdict = dict()
    for i in range(int(header), len(tsvList)):  # Iterate each line in the TSV file (except for th header)

        if (filterOff or float(tsvList[i][filterindex]) < filtercutoff):
            clean = clean_string(tsvList[i][peptidindex])  #  Remove  non-alphabetical sequence amino acids from peptide
            if (clean in rdict):
                rdict[clean] += 1
            else:
                rdict[clean] = 1  # Initialize count by value 1 for new peptide

    return rdict


def clean_string(value):  # Removes all non-alphabetical letters from a string
    return (''.join(i for i in value if
                    i.isalpha()))  # Check each letter in string whether it is alphabetical, else discard it
