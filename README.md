# Hortense
Horizontal gene transfer (HGT) detection using MS/MS proteomics data.

## Requirements (Linux)
- Snakemake https://snakemake.readthedocs.io/en/stable/index.html
- YAML
- Python 3
- MS-GF+

## Setup

Please make sure that Python is installed, and download the database search engine MS-GF+ from the following URL:
http://proteomics.ucsd.edu/Software/MSGFPlus/

You can either install the Python packages for Snakemake and YAML via your favourite package manager, or you can use Miniconda for a local installation.

Downloading and installing Miniconda:
```
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh
```
Installing packages via the bioconda channel (https://bioconda.github.io/) using Miniconda:
```
/path/to/conda/conda install -c bioconda snakemake yaml
```

Checkout/Clone the code from this Gitlab repository. Check your `$PATH` variable for the local Python/Snakemake/YAML installation, if applicable.

## Usage

(i) Create an output directory for your project and place your MS/MS spectrum files in MGF format into a folder called `mgf` in your output directoy. 
(ii) Create the folder `protdb` in your output directory and add proteine databases for acceptor and donor. Please mind the naming conventions: If your `mgf` file is named `sample.mgf`, the protein databases have to be named accordingly (`sample_acceptor.fasta` and `sample_donor.fasta`).
(iii) Adjust the paths to MS-GF+ and the output directory within the `config.yaml` configuration file and start the pipeline via the following command: 
```
snakemake -s /path/to/hortense/hortense.snake --configfile /path/to/config.yaml --cores 10
```

### Example

The example folder contains a complete set of input, output and config files for a simulated HGT example.
- `example/mgf/hpylori.mgf`: **Input** MGF file (required)
- `example/protdb/hpylori_*.fasta`: **Input** Protein database (required) and genome reference files
- `example/mzid/hpylori_*.mzid`: Intermediate MS-GF+ result files
- `example/tsv/hpylori_*.tsv`: Intermediate tsv-converted MS-GF+ result files
- `example/hortense/hpylori_*.csv`: Final Hortense result files
- `example/hpylori.yaml`: Example config file

From within the example folder, you can reproduce the results by calling
```
snakemake -s /path/to/hortense/hortense.snake --configfile hpylori.yaml --cores 1
```
**Important**: Snakemake automatically checks for existing (temporary) result files, hence, by downloading all example result files, there is per se nothing to do for snakemake. To rerun either delete the result files you want to reproduce or force snakemake to overwrite existing files by calling
```
snakemake -s /path/to/hortense/hortense.snake --configfile hpylori.yaml --cores 1 --forceall
```
