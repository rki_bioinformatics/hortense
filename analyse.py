"""
Created on Wed Jul  6 13:02:07 2016

@author: WulfB
"""
#### Hortense Analysis

import fasta, proteindb, genome_search
import MSGFPlus as msgf
import datetime
import csv
from collections import Counter


###############################################################################################################################
# run_MSGFPlus - Executes the database search algorithm MS-GF+ on separate donor and acceptor database file.
#
# Parameter:
# fastaFilePathacceptor 	    - Path to the protein database file (FASTA) of the acceptor
# fastaFilePathDonor 	    	- Path to the protein database file (FASTA) of the donor
# msgfOutputAcceptor     		- MSGF+ output file for the acceptor
# msgfOutputDonor   			- MSGF+ output file for the donor
# outputfolder 				    - MS-GF+ output directory
# acceptorFDR			    	- FDR threshold for the acceptor; default 0.01 within the interval [0:1] (float)
# donorFDR			    		- FDR threshold for the donor; default 0.01 within the interval [0:1] (float)
# genomeFilePathDonor	    	- Path to the donor genome file (FASTA); optional parameter used for genome mapping.
# homologyFilter_sharedPeptides	- Homology filtering using shared peptides; default == 3 -> Proteins are considered as homologous when sharing three peptides.
##############################################################################################################################


def run_MSGFPlus(fastaFilePathAcceptor, fastaFilePathDonor, msgfOutputAcceptor, msgfOutputDonor, outputfolder,
                 acceptorFDR=0.01, donorFDR=0.01, genomeFilePathDonor = None, homologyFilter_sharedPeptides=3):
    time = datetime.datetime.now()  # Create timestamp
    timestring = time.strftime("%Y-%m-%d_%H-%M-%S")  # Format timestamp
    print(timestring)
    # Paths to the protein databases (acceptor and donor)
    dbf = {'Acceptor': fastaFilePathAcceptor,
           'Donor': fastaFilePathDonor}  # Create dictionary for faster reading using the function loadfiles from Fasta class
    # Wandele Protein-Datenbanken in Fasta-Listen
    print('Loading Protein databases...')
    dbd = fasta.loadfiles(dbf)
    print("Digesting acceptor database...")

    acceptor = proteindb.ProteinDb(dbd['Acceptor'])  # Performs an in-silico digest of the acceptor protein sequences. Dictionary contains peptide to protein mapping
    print("Digesting donor database...")
    donor = proteindb.ProteinDb(dbd['Donor'])  # Performs an in-silico digest of the donor protein sequences. Dictionary contains peptide to protein mapping
    print("Databases digested!")
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    acceptorPeptides = set(acceptor.getPeptides())  # Builds a set of acceptor peptides (I has been replaced by L)
    donorPeptides = set(donor.getPeptides())  # Builds a set of donor peptides (I has been replaced by L)
    adi = acceptorPeptides.intersection(donorPeptides)  # Builds an intersection of the acceptor or donor peptides
    uniqueAcceptorPeptides = acceptorPeptides.difference(adi)  # Peptides unique for the acceptor
    uniqueDonorPeptides = donorPeptides.difference(adi)  # Peptides unique for the donor
    acceptorData = msgf.Tsv2List(msgfOutputAcceptor)  # Reads MS-GF+ output of the acceptor to construct dataframe

    ## FDR filtering: save spectrum titles if estimated FDR below threshold (acceptor)
    acceptorSpectrumTitles = {row[3] for row in acceptorData[1:] if float(row[15]) < acceptorFDR}
    donorData = msgf.Tsv2List(msgfOutputDonor)  # Reads MS-GF+ output of the donor to construct dataframe
    ## Remove spectra from donor dataframe which were significant within the acceptor
    filteredData = [row for row in donorData if not (row[3] in acceptorSpectrumTitles)]

    # The filtering return all spectrum identifications below the FDR threshold. Each isoleucin (I) is replaced by leucine (L) and peptide set is returned
    peptideData = {peptid.replace('I', 'L') for peptid in
                   msgf.TsvFilter2(filteredData, filtercutoff=donorFDR, header=True).keys()}
    print("Number of peptides in sample data:", len(peptideData))

    # Assure that only peptides are taken over which derive from the database
    peptideData = (acceptorPeptides.union(donorPeptides)).intersection(peptideData)
    # Select peptides which only occur in the donor.
    peptidesDonorOnly = peptideData.intersection(uniqueDonorPeptides)
    print("Number of peptides in acceptor database:", len(acceptorPeptides))
    print("Number of peptides in donor database:\t", len(donorPeptides))
    print('Number of peptides only in donor:\t\t\t', len(peptidesDonorOnly))
    print('Number of peptides only in acceptor:\t', len(peptideData.intersection(uniqueAcceptorPeptides)))
    print('Number of peptides shared between the databases:\t\t', len(peptideData.intersection(adi)))
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    print('Searching for donor proteins with unique peptide-spectrum-matches')

    # Create subset of donor database with actually occurring proteins (speeding up the process)
    subsetDonorDatabase = donor.keys2subDB(peptidesDonorOnly)

    do_query_unambiguous = [key for key in subsetDonorDatabase.db if len(
        set(subsetDonorDatabase.db[key])) == 1]  # Select only peptides which identify a protein unambiguously
    print("Number of unambiguous donor peptides:", len(do_query_unambiguous))
    subsetDonorDatabase = donor.keys2subDB(do_query_unambiguous)
    donorProteins = subsetDonorDatabase.getProteins()

    print("Found %i proteins with unique donor PSMs" % len(donorProteins))
    rlist = list()  # Results for the homology filtering.

    if (homologyFilter_sharedPeptides):
        print("Searching for homologous proteins in acceptor")
        ds2 = subsetDonorDatabase  # Create copy of subset
        ds2.trim()  # Remove unreachable proteins
        # Select all donor peptides and create a subset of the acceptor with the proteins at which donor peptides occur
        subsetAcceptorDatabase = acceptor.keys2subDB(set(ds2.getPeptides()))

        for i in donorProteins:
            p = set(i.getNormalPeptides())  # Retrieve peptide set for each donor protein
            As = subsetAcceptorDatabase.keys2subDB(p)  # Create a subset of reachable proteins within acceptor
            value = list(As.db.values())
            numbers = list()

            for e in value:  # Add reachable proteins to list
                for f in e:
                    numbers.append(f)

            # Checks how many peptides are shared for homology filtering
            if max(Counter(numbers) or [0]) <= homologyFilter_sharedPeptides:
                rlist.append(i)  # Keep protein

    else:
        rlist = donorProteins  # Without using the homology filtering

    print("Found %i unambiguous proteins\nCalculate protein Coverage" % len(rlist))

    qc = proteindb.ProteinDb(rlist).keys2subDB(peptideData)  # Add the proteins belonging to the donor to new protein database
    d = qc.coverage()  # Calculate the coverage
    print("Write protein sequences to %s" % (outputfolder + '_protein_seqs.fasta'))
    fasta.write(rlist, outputfolder + '_protein_seqs.fasta')  # Write found proteins to the FASTA output file

    covdict = dict((e[0].header, e[1]) for e in d)  # Create a mapping with proteins of which coverage was calculated
    p = dict((i.header, ';'.join(set(i.getNormalPeptides()).intersection(peptidesDonorOnly))) for i in rlist)
    number_peptides = dict((i, len(p[i].split(';')))  for i in p.keys())

    if (genomeFilePathDonor is not None):
        print("Load donor genome")
        genom = fasta.read(genomeFilePathDonor)
        r = dict()
        c = 0
        for header in genom:  # For each header within the genome FASTA file
            c += 1
            r[header] = genome_search.find_all(genom[header], rlist)  # Search all proteins within six-frame translation
            print(c, ". search completed.")
            x = genome_search.append_info(r[header], header)  # Add header of reference genome to the result
            x = genome_search.append_info_dict(x, covdict)  # Add coverage to the result
            x = genome_search.append_info_dict(x, p)  # Add used peptides to the result
            print(len(x))
            # TODO make sure all results are appended to same file and previous content is not overwritten
            genome_search.write_occurrence(x, outputfolder + "_occurrence.csv", ['Chromosom', 'Coverage', 'Peptide'])

    print(
        "Write proteins with sequence coverage and peptides to %s" % (outputfolder + "_protein_coverage_peptides.csv"))
    l = [[protein.header, protein.sequence, covdict[protein.header], number_peptides[protein.header], p[protein.header]] for protein in
         rlist]  # Another file is create with protein header, sequence coverages and associated peptides
    hl = [['Header', 'Sequence', 'Coverage', '# Peptides', 'Peptides']] + l
    with open(outputfolder + "_protein_coverage_peptides.csv", "w") as f:
        writer = csv.writer(f)
        writer.writerows(hl)  # Write list to CSV file
    f.close()
    print('Finish!')

