import csv

##################################################
## Search donor genome for protein or peptide   ##
## sequences from HGT region (6 FT)             ##
##################################################
codontable = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W'
    }


reversedna = {'A':'T','C':'G','G':'C','T':'A','N':'N'}


############################################################
## translate(dna,shift=1)                                 ##
## Translate dna to protein                               ##
## shift 1,2,3: reading frames starting with base 0,1,2   ##
## shift -1,-2,-3: reverse complement reading frames      ##
############################################################
def translate(dna, shift = 1):

    # Sanity check
    if (shift == 0 or shift < -3 or shift > 3):
        raise ValueError('wrong shift value: must be -3,-2-1,1,2,3')
    # Reverse complement dna for negative shift
    if (shift < 0):
        dna = dna[::-1]
        dna = "".join([reversedna.get(i,i) for i in dna])
    # Use codon table to translate dna letters using the current frame
    return(''.join([codontable.get(dna[i:i+3],'X') for i in range(abs(shift)-1,len(dna)-2,3)]))

###################################################
## find_all(chr, prot_list)                      ##
## Returns list of prot-seq hits on chromosome   ##
## [[Protein,Protein-Sequence,Start,Stop,Frame]] ##
###################################################
def find_all(chr, prot_list):
    gen_list = list()
    shift = [1,2,3,-1,-2,-3]
    for frame in shift:
        prot_seq = translate(chr, frame)
        # Reverse complement
        if frame < 0:
            offset = len(chr) + frame + 1
            for protein in prot_list:
                start = 0
                while True:
                    # Search for prot_seq w/o start codon (methionin)
                    start = prot_seq.find(protein.sequence[1:], start)
                    # Offset/shift in start position w/ (1) or w/o (0) methionin
                    methionin_offset = 1
                    # prot_seq not found
                    if (start == -1):
                        break
                    # check if protein has start codon in dna seq, if not reset methionin offset
                    if (start > 0 and prot_seq[start - 1] == protein.sequence[0]):
                        methionin_offset = 0
                    # Save protein header, sequence, first and last base, and frame
                    gen_list.append([protein.header, protein.sequence[methionin_offset:], offset - 3*(start - 1 + methionin_offset) - 1, offset - 3*(start + len(protein.sequence) - 1), frame])
                    start += 1

                
        # Forward
        else:
            offset = frame - 1
            for protein in prot_list:
                start = 0
                while True:
                    start = prot_seq.find(protein.sequence[1:], start)
                    methionin_offset = 1
                    # prot_seq not found
                    if (start == -1):
                        break
                    if (start > 0 and prot_seq[start - 1] == protein.sequence[0]):
                        methionin_offset = 0
                    # Save protein header, sequence, first and last base, and frame
                    gen_list.append([protein.header, protein.sequence, 3*(start - 1 + methionin_offset) + offset, 3*(start + len(protein.sequence) - 1) + offset - 1, frame])
                    start += 1
    return(gen_list)

	
	
##################################################
## find_peptide(dna, peptide_list               ##
## Returns peptides found in 6FT of chromosome  ##
##################################################
def find_peptide(dna, peptide_list):
    found_peptides = set()
    shift = [1,2,3,-1,-2,-3]
    for s in shift:
        prot_seq = translate(dna, s)
 
        for peptide in peptide_list:
            if(prot_seq.find(peptide) >= 0):
                found_peptides.add(peptide)
    return(found_peptides)


#####################################################################
## pgc(protein, genome)                                  		   ##
## protein/genome sanitiy check for database: returns number of    ##
## proteins *not* found in given genome sequence (optimal: 0)      ##
#####################################################################
def pgc(protein, genome):
    # Set of protein seqs w/o start codon
    prot_seqs = {i.sequence[1:] for i in protein}
    missing_proteins = set()
    for chromosome in genome:
        missing = prot_seqs.difference(missing_proteins)
        missing_proteins = missing_proteins.union(find_peptide(genome[chromosome], missing))
    return (len(prot_seqs) - len(missing_proteins))


##########################################################################
## append_info_dict(results,Infos=dict())            		        	##
## Extends (protein) list (from find_all) w/ furhter info from dict()	##
##########################################################################
def append_info_dict(results, Infos = dict()):
    extended_list = list()
    for res in results:
        extended_list.append(res + [Infos.get(res[0], '')])
    return extended_list


##########################################################################
## appendValue2all(results,Info='')				         		      	##
## Extends (protein) list (from find_all) w/ furhter single info 		##
##########################################################################
def append_info(results, Info = ''):
    extended_list = list()
    for res in results:
        extended_list.append(res + [Info])
    return extended_list

##########################################################################
## write_occurrence(occurrence,path="./occurrence.csv",extra=[])   	   	##
## Writes the results of function find_all to CSV.                      ##
##########################################################################
def write_occurrence(occurrence, path = "./occurrence.csv", extra = []):
    
    sh = [['Header','Protein','Start','Stop','Frame'] + extra] + occurrence
    with open(path, "w") as f:
        writer = csv.writer(f)
        writer.writerows(sh)
    f.close()




    
