############
# Packages #
############
import datetime
import os
import platform
import pwd
import socket
import sys

########
# Info #
########
EXEC_TIME = datetime.datetime.strftime(datetime.datetime.now(), '%d-%m-%y_%H-%M')
PYTHON_VERSION = sys.version.split(' ')[0]
USER = pwd.getpwuid(os.getuid()).pw_name
SERVER = socket.getfqdn()
CALL = ' '.join(sys.argv)
if platform.system() == 'Linux':
    MACHINE = '{} {}'.format(platform.linux_distribution()[0], platform.linux_distribution()[1])
elif platform.system() == 'Windows':
    MACHINE = '{} {} {}'.format(platform.win32_ver()[0], platform.win32_ver()[1], platform.win32_ver()[2])
elif platform.system() == 'Java':
    MACHINE = 'Java'
else:
    MACHINE = '{} {}'.format(platform.mac_ver()[0], platform.mac_ver()[1][0])
__version__ = '0.0.1'


##########
# Config #
##########
try:
    os.makedirs(config['outputdir'], exist_ok=True)
except:
    pass
LOG = os.path.realpath(os.path.join(config['outputdir'], 'log-{}.txt'.format(EXEC_TIME)))

##############
### Script ###
##############

print(config['outputdir'])
print(expand(os.path.join(config['outputdir'], 'hortense', '{sample}_{donorid}_protein_coverage_peptides.csv'), sample=glob_wildcards(config['outputdir'] + '/mgf/' + '{sample}.{ext, (mgf|mzXML)}')[0], donorid=glob_wildcards(config['outputdir'] + '/donrefs/' + '{donorid}.fasta')[0]))

def getPrefix(wildcards):
    print (wildcards)
    return (wildcards["sample"])

def getHomfilter(homfilter):
    print (homfilter)
    if (homfilter > 0):
        return '-f ' + str(homfilter)
    else:
        return ''

def getReference(altrefprefix, reference):
    if not altrefprefix:
        return os.path.join(config['outputdir'], 'protdb', '{sample}_{protdb}.fasta')
    else:
        return os.path.join(config['outputdir'], 'protdb', altrefprefix + '_' + reference + '.fasta')

def getDonReference(altrefprefix, donorid):
    if not altrefprefix:
        return os.path.join(config['outputdir'], 'protdb', '{sample}_{donorid}_donor.fasta')
    else:
        return os.path.join(config['outputdir'], 'protdb', altrefprefix + '_' + donorid + '_donor.fasta')

def filter_go_files(wc):
    filter_names, = glob_wildcards(os.path.join(config['outputdir'], 'donrefs', '{ids}.fasta'))
    files = [os.path.join(config['outputdir'], 'protdb', wc_sample + '_' + ids + "_donor.fasta") for ids in sorted(filter_names)]
    return(files)

def get_donor_ids(wc):
    filter_names, = glob_wildcards(os.path.join(config['outputdir'], 'donrefs', '{ids}.fasta'))
    return(filter_names)

# non-hard-coded MSGF+ parameters are given in config.yaml
if config['mult_donor']:
    rule all:
        input:
            #expand(os.path.join(config['outputdir'], 'hortense', '{sample}_{donorid}_protein_coverage_peptides.csv'), sample=glob_wildcards(config['outputdir'] + '/mgf/' + '{sample}.mgf')[0], donorid=glob_wildcards(config['outputdir'] + '/donrefs/', '{donorid}.fasta')[0]) 
            expand(os.path.join(config['outputdir'], 'hortense', '{sample}_{donorid}_protein_coverage_peptides.csv'), sample=glob_wildcards(config['outputdir'] + '/mgf/' + '{sample}.mgf')[0], donorid=glob_wildcards(config['outputdir'] + '/donrefs/' + '{donorid}.fasta')[0])
            #"hortense/{sample}_occurrence.csv"
            #expand('hortense/{sample}_occurrence.csv', sample=glob_wildcards('mgf/' + '{sample}.mgf{ext,[^/]*}')[0])
        
    rule msgfplus_mzid_mgf_acceptor_mult:
        input:
            mgf = os.path.join(config['outputdir'], 'mgf', '{sample}.mgf'),
            fasta = os.path.join(config['outputdir'], 'protdb', '{sample}_acceptor.fasta')
        output: 
            os.path.join(config['outputdir'], 'mzid', '{sample}_acceptor.mzid')
        threads: config['threads']
        shell:
            "java {config[msgfplusmem]} -jar {config[msgfplus]} -s {input.mgf} -d {input.fasta} -thread 20 -t {config[tolerance]} -tda 1 -inst 1 -e {config[enzyme]} -m {config[model]} -o {output} 2>&1 >> {LOG}"


    rule msgfplus_mzid_mgf_donor_mult:
        input:
            mgf = os.path.join(config['outputdir'], 'mgf', '{sample}.mgf'),
            protdb_file = os.path.join(config['outputdir'], 'protdb', '{sample}_{donorid}_donor.fasta') #protdb_files = filter_go_files #getDonReference(config['alternative-ref-prefix'], '{donorid}') #os.path.join(config['outputdir'], 'protdb', '{sample}_{protdb}.fasta')
        output: 
            os.path.join(config['outputdir'], 'mzid', '{sample}_{donorid}_donor.mzid') #, donorid=get_donor_ids)
        threads: config['threads']
        shell:
            "java {config[msgfplusmem]} -jar {config[msgfplus]} -s {input.mgf} -d {fasta} -thread 20 -t {config[tolerance]} -tda 1 -inst 1 -e {config[enzyme]} -m {config[model]} -o {output} 2>&1 >> {LOG}"
        #run:
        #    for fasta in filter_go_files:
        #        shell("java {config[msgfplusmem]} -jar {config[msgfplus]} -s {input.mgf} -d {fasta} -thread 20 -t {config[tolerance]} -tda 1 -inst 1 -e {config[enzyme]} -m {config[model]} -o {output} 2>&1 >> {LOG}")


    rule msgfplus_tsv_acceptor_mult:
        input:
            os.path.join(config['outputdir'], 'mzid', '{sample}_acceptor.mzid')
        output:
            os.path.join(config['outputdir'], 'tsv', '{sample}_acceptor.tsv')
        shell:
            "java {config[msgfplusmem]} -cp {config[msgfplus]} edu.ucsd.msjava.ui.MzIDToTsv -i {input} -o {output} 2>&1 >> {LOG}"

    rule msgfplus_tsv_donor_mult:
        input:
            os.path.join(config['outputdir'], 'mzid', '{sample}_{donorid}_donor.mzid')
        output:
            os.path.join(config['outputdir'], 'tsv', '{sample}_{donorid}_donor.tsv')
        shell:
            "java {config[msgfplusmem]} -cp {config[msgfplus]} edu.ucsd.msjava.ui.MzIDToTsv -i {input} -o {output} 2>&1 >> {LOG}"


    rule hortense_mult:
        input:
            accfasta = os.path.join(config['outputdir'], 'protdb', '{sample}_acceptor.fasta'),
            donfasta = os.path.join(config['outputdir'], 'protdb', '{sample}_{donorid}_donor.fasta'),
            acctsv = os.path.join(config['outputdir'], 'tsv', '{sample}_acceptor.tsv'),
            dontsv = os.path.join(config['outputdir'], 'tsv', '{sample}_{donorid}_donor.tsv')
        output:
            os.path.join(config['outputdir'], 'hortense', '{sample}_{donorid}_protein_seqs.fasta'),
            os.path.join(config['outputdir'], 'hortense', '{sample}_{donorid}_protein_coverage_peptides.csv')
        params:
            prefix = os.path.join(config['outputdir'], 'hortense', '{sample}_{donorid}'),
            homfilter = getHomfilter(config['homfilter'])
        shell:
            "python {config[hortense]} -a {input.accfasta} -d {input.donfasta} --msgfplus_acceptor {input.acctsv} --msgfplus_donor {input.dontsv} -o {params.prefix} {params.homfilter} --fdr_donor 0.001 --fdr_acceptor 0.001 2>&1 >> {LOG}"
else:
    rule all:
        input:
            #"hortense/{sample}_occurrence.csv"
            expand(os.path.join(config['outputdir'], 'hortense', '{sample}_protein_coverage_peptides.csv'), sample=glob_wildcards(config['outputdir'] + '/mgf/' + '{sample}.{ext, (mgf|mzXML)}')[0])
            #expand('hortense/{sample}_occurrence.csv', sample=glob_wildcards('mgf/' + '{sample}.mgf{ext,[^/]*}')[0])

        

    rule msgfplus_mzid_mgf:
        input:
            mgf = os.path.join(config['outputdir'], 'mgf', '{sample}.mgf'),
            fasta = getReference(config['alternative-ref-prefix'], '{protdb}') #os.path.join(config['outputdir'], 'protdb', '{sample}_{protdb}.fasta')
        output: 
            os.path.join(config['outputdir'], 'mzid', '{sample}_{protdb}.mzid')
        threads: config['threads']
        shell:
            "java {config[msgfplusmem]} -jar {config[msgfplus]} -s {input.mgf} -d {input.fasta} -thread 20 -t {config[tolerance]} -tda 1 -inst 1 -e {config[enzyme]} -m {config[model]} -o {output} 2>&1 >> {LOG}"

    rule msgfplus_mzid_mzxml:
        input:
            mgf = os.path.join(config['outputdir'], 'mgf', '{sample}.mzXML'),
            #fasta = os.path.join(config['outputdir'], 'protdb', '{sample}_{protdb}.fasta')
            fasta = getReference(config['alternative-ref-prefix'], '{protdb}') #os.path.join(config['outputdir'], 'protdb', '{sample}_{protdb}.fasta')
        output: 
            os.path.join(config['outputdir'], 'mzid', '{sample}_{protdb}.mzid')
        threads: config['threads']
        shell:
            "java {config[msgfplusmem]} -jar {config[msgfplus]} -s {input.mgf} -d {input.fasta} -thread 20 -t {config[tolerance]} -tda 1 -inst 1 -e {config[enzyme]} -m {config[model]} -o {output} 2>&1 >> {LOG}"

    rule msgfplus_tsv:
        input:
            os.path.join(config['outputdir'], 'mzid', '{sample}_{protdb}.mzid')
        output:
            os.path.join(config['outputdir'], 'tsv', '{sample}_{protdb}.tsv')
        shell:
            "java {config[msgfplusmem]} -cp {config[msgfplus]} edu.ucsd.msjava.ui.MzIDToTsv -i {input} -o {output} 2>&1 >> {LOG}"


    rule hortense:
        input:
            accfasta = getReference(config['alternative-ref-prefix'], 'acceptor'), #os.path.join(config['outputdir'], 'protdb', '{sample}_acceptor.fasta'),
            donfasta = getReference(config['alternative-ref-prefix'], 'donor'), #os.path.join(config['outputdir'], 'protdb', '{sample}_donor.fasta'),
            acctsv = os.path.join(config['outputdir'], 'tsv', '{sample}_acceptor.tsv'),
            dontsv = os.path.join(config['outputdir'], 'tsv', '{sample}_donor.tsv')
        output:
            os.path.join(config['outputdir'], 'hortense', '{sample}_protein_seqs.fasta'),
            os.path.join(config['outputdir'], 'hortense', '{sample}_protein_coverage_peptides.csv')
            #os.path.join(config['outputdir'], 'hortense', '{sample}_occurrence.csv')
        params:
            prefix = os.path.join(config['outputdir'], 'hortense', '{sample}'),
            homfilter = getHomfilter(config['homfilter']),
            donfastag = getReference(config['alternative-ref-prefix'], 'donorg') #os.path.join(config['outputdir'], 'protdb', '{sample}_donorg.fasta')
        run:
            if config['gen_mapping']:
                shell("python {config[hortense]} -a {input.accfasta} -d {input.donfasta} --msgfplus_acceptor {input.acctsv} --msgfplus_donor {input.dontsv} -o {params.prefix} {params.homfilter} -g {params.donfastag} --fdr_donor 0.001 --fdr_acceptor 0.001 2>&1 >> {LOG}")
            else:
                shell("python {config[hortense]} -a {input.accfasta} -d {input.donfasta} --msgfplus_acceptor {input.acctsv} --msgfplus_donor {input.dontsv} -o {params.prefix} {params.homfilter} --fdr_donor 0.001 --fdr_acceptor 0.001 2>&1 >> {LOG}")


onstart:
    with open(LOG, 'a') as f:
        f.write('Version: {}\n'.format(__version__))
        f.write('Execution time: {}\n'.format(EXEC_TIME))
        f.write('Python Version: {}\n'.format(PYTHON_VERSION))
        f.write('User: {}\n'.format(USER))
        f.write('Server: {}\n'.format(SERVER))
        f.write('Machine: {}\n'.format(MACHINE))
        f.write('Call: {}\n'.format(CALL))

onsuccess:
    shell('cat {log} >> {LOG}')

onerror:
    shell('cat {log} >> {LOG}')
