################################################################################################
#   Wrapper class FastaEntry is used to read protein sequences in FASTA format.                #
#   Contains helper method to split protein sequences into peptides (tryptic cleavage only).   #
################################################################################################
class FastaEntry:
    def __init__(self, header, sequence="", comment=''):
        self.header = header.rstrip('\r\n')
        self.sequence = sequence.rstrip('\r\n')
        self.comment = comment

    def __str__(self):
        return ("Header: " + self.header + "\n\nSequence:" + self.sequence)

    def __repr__(self):
        return "Header:" + self.header + ":" + self.sequence[1:30] + ".."

    def getSimplePeptites(self):  # Cuts protein sequences after K and R and returns a list of ordered peptides.
        s = self.sequence.replace('R', 'R42')  # Replaces R by R42 since the function split removes the delimiter.
        s = s.replace('K', 'K42')  # Replaces K by K42 since the function split removes the delimiter.
        if (s[-2:] == "42"):  # Remove 42 if it is in the beginning.
            s = s[:-2]

        return (s.split('42'))

    def getNormalPeptides(self, maxCleavages=float('Inf'), minPeptideLength=6, maxPeptideLength=40):  # Generates peptides with length between 6 an 40 (with missed cleavages).
        sp = self.getSimplePeptites()
        np = list()
        for i in range(0, len(sp)):
            for j in range(i + 1, len(sp) + 1):
                if (maxCleavages < j - i - 1):  # Break loop, if maximum amount of missed cleavages has been reached.
                    break
                t = ''.join(sp[i:j])
                if (len(t) >= minPeptideLength and (len(t) <= maxPeptideLength)):
                    np.append(t)
                if (len(t) > maxPeptideLength):
                    break


        sp[0] = sp[0][1:]  # Remove the start codon
        for i in range(1, len(sp) + 1):
            withoutstart = ''.join(sp[0:i])
            if (len(withoutstart) <= maxPeptideLength and i <= maxCleavages):  # Break if maximum length or maximum amount of missed cleavages has been reached.
                if (len(withoutstart) >= minPeptideLength):
                    np.append(withoutstart)
            else:
                break
        ret = {e.replace('I', 'L') for e in np}
        return ret


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# read(filepath)                                                     #
# Reads FASTA file and returns a dictionary with sequence headers    #
# as keys and sequences as values.                                   #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def read(filepath):
    fasta = dict()
    header = 'none'  # Current header
    with open(filepath) as infile:
        for line in infile:
            if (line[0] == '>'):  # Begin header line with '>'
                header = line.strip('\r\n')
                fasta[header] = []

            elif (line[0] == '#'):
                pass

            else:
                fasta[header].append(
                    line.strip('\r\n'))

    for key in fasta:
        fasta[key] = ''.join(fasta[key])

    return fasta


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# fasta2FastaProt(filepath)                                             #
# Reads a file in FASTA format and returns a list of FastaProt objects. #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def fasta2FastaProt(filepath):
    fastaList = list()
    header = ''
    seq = []
    com = []
    first = True
    with open(filepath) as infile:
        for line in infile:
            if (line[0] == '>'):
                if (first):
                    first = False

                else:
                    s = ''.join(seq)  # Concatenate sequence lines for each new header.
                    c = ''.join(com)  # The same for the comments.

                    fastaList.append(
                        FastaEntry(header, s, c))  # Add header, sequence and comments to return list.

                header = line.strip('\r\n')
                seq = []
                com = []

            elif (line[0] == '#'):
                com.append(line)

            else:
                seq.append(line.strip('\r\n'))

        s = ''.join(seq)
        c = ''.join(com)
        fastaList.append(FastaEntry(header, s, c))

    return fastaList

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# loadfiles(dateien, rmdupl=False)                                   #
# Reads several FASTA protein files into dictionary.                  #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def loadfiles(files, rmdupl=False):
    orga = dict()
    for f in files:
        proteins = fasta2FastaProt(files[f])
        if (rmdupl):  # Remove duplicates?
            proteins_nr = list()  # Create new return list without duplicates
            sequenceSet = set()
            for elem in proteins:
                if (not (elem.sequence in sequenceSet)):
                    sequenceSet.add(elem.sequence)
                    proteins_nr.append(elem)
            orga[f] = proteins_nr
        else:
            orga[f] = proteins
    return orga


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# insert_newlines(string, stepSize=70)                               #
# Inserts new line every 70 letters (default stepSize value).        #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def insert_newlines(string, stepSize=70):
    return '\n'.join(string[i:i + stepSize] for i in range(0, len(string), stepSize))


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
# write(FastaList,filepath="./")                                     #
# Writes FastaList back to FASTA file.                               #
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#
def write(fastaList, filepath="./"):
    file = open(filepath, "w")
    for elem in fastaList:
        file.write(elem.header)
        file.write("\n")
        file.write(elem.comment)
        file.write("\n")
        file.write(insert_newlines(elem.sequence))
        file.write("\n")

    file.close()