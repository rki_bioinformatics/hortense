#!/usr/bin/env python3
import getopt
import sys
import analyse
import argparse

version='0.1'

def validFloat(f):
    if (float(f) < 0 or float(f) > 1):
        raise argparse.ArgumentTypeError("FDR limit exceeds limit (range 0-1)")
    return f

def parser():
    parser = argparse.ArgumentParser(fromfile_prefix_chars='@',description='Hortense - HGT detection from proteomics MS data',add_help=False)    
    required = parser.add_argument_group('required parameters')
    optional = parser.add_argument_group('optional parameters')
    optional.add_argument('-h', '--help', action='help', help='Show this help message and exit')
    required.add_argument('-a', '--acceptor', dest='acceptor', nargs='?', type=str, required=True, default=None, help='Acceptor protein sequences (fasta)')
    required.add_argument('-d', '--donor', dest='donor', nargs='?', type=str, required=True, default=None, help='Donor protein sequences (fasta)')
    required.add_argument('-ma', '--msgfplus_acceptor', dest='msgf_acceptor', nargs='?', type=str, required=True, default=None, help='MSGF+ output in tsv format for acceptor')
    required.add_argument('-md', '--msgfplus_donor', dest='msgf_donor', nargs='?', type=str, required=True, default=None, help='MSGF+ output in tsv format for donor')
    required.add_argument('-o', '--output', dest='output', nargs='?', type=str, required=True, default='sample1', help='Sample specific prefix for output files')
    optional.add_argument('-g', '--genome_donor', dest='genome_donor', nargs='?', type=str, help='Donor genome sequence (fasta)')
    optional.add_argument('-fa', '--fdr_acceptor', dest='fdr_acceptor', nargs='?', type=validFloat, default=0.01, help='FDR limit for acceptor (default 0.01)')
    optional.add_argument('-fd', '--fdr_donor', dest='fdr_donor', nargs='?', type=validFloat, default=0.01, help='FDR limit for donor (default 0.01)')
    optional.add_argument('-f', '--hom_filter', dest='homfilter', nargs='?', type=int, default=0, help='Number of shared peptides in homology filter (default 0=filter off)')
    return parser

def main(args):
    
    
    print('Hortense Version: '+ version)
    print(args.genome_donor)
    analyse.run_MSGFPlus(args.acceptor, args.donor, args.msgf_acceptor, args.msgf_donor, args.output, float(args.fdr_acceptor), float(args.fdr_donor), args.genome_donor, args.homfilter)

if __name__ == "__main__":
    args = parser().parse_args()
    main(args)

